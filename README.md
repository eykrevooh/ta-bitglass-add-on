# TA-Bitglass-Add-On
Bitglass add-on for Splunk Enterprise and Splunk Cloud

# ABOUT THIS APP

* The Technology Add-on for Bitglass is used to download data from cloud.bitglass.com, do parsing and indexing on it.
* This is an add-on initially genereated by the Splunk Add-on Builder.
* This Add-on uses Splunk KV store for checkpoint mechanism.

# REQUIREMENTS

* Splunk version 7.0, 7.1, 7.2, 7.3 and 8.0
* Appropriate API token for collecting data from cloud.bitglass.com

# Release Notes

## Version: 1.0.0
* Initial release of App compatible with Python2 and Python3

# OPEN SOURCE COMPONENTS AND LICENSES
* dateutil version 2.6.1 https://pypi.org/project/python-dateutil/ (LICENSE https://github.com/dateutil/dateutil/blob/master/LICENSE)
* croniter version 0.3.25 https://pypi.org/project/croniter/ (LICENSE https://github.com/kiorky/croniter/blob/master/docs/LICENSE)
* arrow 0.15.2 https://pypi.org/project/arrow/ (LICENSE https://github.com/crsmithdev/arrow/blob/master/LICENSE)


# INSTALLATION OF APP

* This Add-on can be installed through UI using "Manage Apps" or extract zip file directly into /opt/splunk/etc/apps/ folder.

# CONFIGURATION OF APP

* Navigate to Bitglass Add-on for Splunk, click on "Configuration" page, go to "Account" tab and then click "Add", fill in "Account Name", "Address", "Verify SSL Certificate" and provide a valid Bitglass API Auth Token

* Navigate to Bitglass Add-on for Splunk, click on new input fill the "Account Name", "Bitglass Log Type", "Interval", "Index", "Global Account", "Start Time" and "API Version" fields.

# SAMPLE EVENT GENERATOR

* The TA-Biglass-Add-On, comes with sample data files, which can be used to generate sample data for testing. In order to generate sample data, it requires the SA-Eventgen application.
* Typically eventgen is disabled for the TA and it will generate sample data at an interval of 1 hour. You can update this configuration from eventgen.conf file available under $SPLUNK_HOME/etc/apps/TA-Bitglass-Add-On/default/.

# TROUBLESHOOTING

* Environment variable SPLUNK_HOME must be set
* Logging level can be set by navigating to Bitglass Add-on for Splunk, click on "Configuration" page, selecting the "Logging" tab and selecting the appropriate log level. Caution the debug log can be quite verbose.
* To troubleshoot Bitglass mod-input check $SPLUNK_HOME/var/log/splunk/ta_bitglass_add_on_bitglass_input.log file.

# UNINSTALL & CLEANUP STEPS

* Remove $SPLUNK_HOME/etc/apps/TA-Bitglass-Add-On
* Remove $SPLUNK_HOME/var/log/splunk/**ta_bitglass_add_on_bitglass_input.log**
