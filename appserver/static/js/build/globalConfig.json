{
    "meta": {
        "name": "TA-Bitglass-Add-On",
        "displayName": "Bitglass Add-On for Splunk",
        "version": "1.0.0",
        "apiVersion": "3.0.0",
        "restRoot": "TA_bitglass_add_on_for_splunk"
    },
    "pages": {
        "configuration": {
            "title": "Configuration",
            "description": "Set up your add-on",
            "tabs": [
                {
                    "name": "account",
                    "title": "Account",
                    "table": {
                        "header": [
                            {
                                "field": "name",
                                "label": "Account name"
                            }
                        ],
                        "actions": [
                            "edit",
                            "delete",
                            "clone"
                        ]
                    },
                    "entity": [
                        {
                            "field": "name",
                            "label": "Account name",
                            "type": "text",
                            "required": true,
                            "help": "Enter a unique name for this account.",
                            "validators": [
                                {
                                    "type": "string",
                                    "minLength": 1,
                                    "maxLength": 50,
                                    "errorMsg": "Length of Account name should be between 1 and 50"
                                },
                                {
                                    "type": "regex",
                                    "pattern": "^[a-zA-Z]\\w*$",
                                    "errorMsg": "Account name must start with a letter and followed by alphabetic letters, digits or underscores."
                                }
                            ]
                        },
                        {
                            "field": "address",
                            "label": "Address",
                            "type": "text",
                            "help": "Enter the FQDN or IP of your server for this account (ie. cloud.bitgalss.com).",
                            "required": true,
                            "encrypted": false,
                            "validators": [
                                  {
                                      "maxLength": 300,
                                      "type": "string",
                                      "errorMsg": "Length of address should be between 1 and 300",
                                      "minLength": 1
                                  }
                              ]
                        },
                        {
                            "label": "Verify SSL Certificate",
                            "required": false,
                            "defaultValue": true,
                            "type": "checkbox",
                            "help": "Should we verify your SSL certificate?",
                            "field": "verify_ssl"
                        },
                        {
                            "label": "Bitglass Auth Token",
                            "type": "text",
                            "help": "Enter the Auth Token for this account.",
                            "field": "auth_token",
                            "required": true,
                            "encrypted": true,
                            "validators": [
                                {
                                    "maxLength": 8192,
                                    "type": "string",
                                    "errorMsg": "Length of access key should be between 1 and 8192",
                                    "minLength": 1
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "logging",
                    "title": "Logging",
                    "entity": [
                        {
                            "field": "loglevel",
                            "label": "Log level",
                            "type": "singleSelect",
                            "options": {
                                "disableSearch": true,
                                "autoCompleteFields": [
                                    {
                                        "label": "DEBUG",
                                        "value": "DEBUG"
                                    },
                                    {
                                        "label": "INFO",
                                        "value": "INFO"
                                    },
                                    {
                                        "label": "WARNING",
                                        "value": "WARNING"
                                    },
                                    {
                                        "label": "ERROR",
                                        "value": "ERROR"
                                    },
                                    {
                                        "label": "CRITICAL",
                                        "value": "CRITICAL"
                                    }
                                ]
                            },
                            "defaultValue": "INFO"
                        }
                    ]
                }
            ]
        },
        "inputs": {
          "title": "Inputs",
          "description": "Manage your data inputs",
          "table": {
              "header": [
                  {
                      "field": "name",
                      "label": "Name"
                  },
                  {
                      "field": "interval",
                      "label": "Interval"
                  },
                  {
                      "field": "index",
                      "label": "Index"
                  },
                  {
                      "field": "disabled",
                      "label": "Status"
                  }
              ],
              "moreInfo": [
                  {
                      "field": "name",
                      "label": "Name"
                  },
                  {
                      "field": "log_type",
                      "label": "Log Type"
                  },
                  {
                      "field": "interval",
                      "label": "Interval"
                  },
                  {
                      "field": "index",
                      "label": "Index"
                  },
                  {
                      "field": "disabled",
                      "label": "Status"
                  },
                  {
                      "label": "Global Account",
                      "field": "global_account"
                  },
                  {
                      "label": "Start Time",
                      "field": "start_time"
                  },
                  {
                      "label": "API Version",
                      "field": "api_version"
                  }
              ],
              "actions": [
                  "edit",
                  "enable",
                  "delete",
                  "clone"
              ]
          },
          "services": [
              {
                "name": "bitglass_input",
                "title": "Bitglass Input",
                "entity": [
                    {
                        "validators": [
                            {
                                "pattern": "^[a-zA-Z]\\w*$",
                                "type": "regex",
                                "errorMsg": "Input Name must start with a letter and followed by alphabetic letters, digits or underscores."
                            },
                            {
                                "minLength": 1,
                                "type": "string",
                                "errorMsg": "Length of input name should be between 1 and 100",
                                "maxLength": 100
                            }
                        ],
                        "type": "text",
                        "label": "Account Name",
                        "field": "name",
                        "help": "Enter a unique name for the data input",
                        "required": true
                    },
                    {
                        "required": false,
                        "help": "Biglass Logs to Ingest",
                        "type": "singleSelect",
                        "required": true,
                        "field": "log_type",
                        "label": "Bitglass Log Type",
                        "options":{
                          "autoCompleteFields":[
                            {
                              "value":"access",
                              "label":"Access"
                            },
                            {
                              "value":"admin",
                              "label":"Admin"
                            },
                            {
                              "value":"cloudaudit",
                              "label":"Cloud Audit"
                            },
                            {
                              "value":"cloudsummary",
                              "label":"Cloud Summary"
                            }
                          ]
                        }
                    },
                    {
                        "type": "text",
                        "label": "Interval",
                        "field": "interval",
                        "help": "Time interval of input in seconds or cron schedule.",
                        "required": true
                    },
                    {
                        "validators": [
                            {
                                "minLength": 1,
                                "type": "string",
                                "errorMsg": "Length of index name should be between 1 and 80.",
                                "maxLength": 80
                            }
                        ],
                        "type": "singleSelect",
                        "label": "Index",
                        "field": "index",
                        "defaultValue": "default",
                        "required": true,
                        "options": {
                            "endpointUrl": "data/indexes",
                            "blackList": "^_.*$",
                            "createSearchChoice": true
                        }
                    },
                    {
                        "field": "global_account",
                        "required": true,
                        "type": "singleSelect",
                        "options": {
                            "referenceName": "account"
                        },
                        "label": "Global Account",
                        "help": ""
                    },
                    {
                        "required": false,
                        "help": "The date (UTC in \"YYYY-MM-DDThh:mm:ssZ\" format) from when to start collecting the data. Default value taken will be start of epoch time.",
                        "type": "text",
                        "validators": [
                            {
                                "pattern": "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$",
                                "type": "regex",
                                "errorMsg": "Time shoud be in YYYY-MM-DDThh:mm:ssZ format."
                            }
                        ],
                        "field": "start_time",
                        "label": "Start Time"
                    },
                    {
                        "required": false,
                        "help": "Biglass API version to be added in the requests cv field",
                        "type": "text",
                        "defaultValue": "1.0.1",
                        "field": "api_version",
                        "label": "API Version"
                    }
                ]
              }
          ]
        }
    }
}
