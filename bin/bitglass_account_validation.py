import os
import io
import sys
import json
import requests
import six.moves.configparser

import splunk.admin as admin
import splunk.entity as entity
from splunk.clilib.bundle_paths import make_splunkhome_path
from splunktaucclib.rest_handler.endpoint.validator import Validator
from bitglass_utility import get_app_version


class GetSessionKey(admin.MConfigHandler):
    def __init__(self):
        self.session_key = self.getSessionKey()

class Address(Validator):
    def __init__(self, *args, **kwargs):
        """

        :param validator: user-defined validating function
        """
        super(Address, self).__init__()
        self._args = args
        self._kwargs = kwargs

    def validate(self, value, data):
        return True
