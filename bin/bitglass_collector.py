# python imports
import time
import json
import arrow
import bitglass_utility as utility
import base64

import datetime
import requests
import sys

import splunk.rest as rest

TIME_PATTERN = "%Y-%m-%dT%H:%M:%SZ"

class BitglassCollector(object):
    """Base class for the bitglass collector.
    This class sets the common input params.
    Along with that it also waits on kvstore to be in ready state,
    because the add-on uses it for maintaining the time checkpoints.
    """

    def __init__(self, helper, ew, analysis_type=None):
        """
        Args:
            helper (object): object of BaseModInput class
            ew (object): object of event writer class
            analysis_type (str, optional): type of analysis e.g. vuln or mobile.
                                    Defaults to None.
        """
        self.helper = helper
        self.event_writer = ew
        self.app_name = helper.get_app_name()
        self.build = utility.get_app_version(self.app_name)
        self.analysis_type = analysis_type
        self._wait_for_kvstore()
        self._set_input_data()
        self.current_time = int(time.time())
        self.last_time = ""

    def _set_input_data(self):
        """Set common imput form fields for data collection.
        """
        self.input_name = self.helper.get_input_stanza_names()
        self.interval = self.helper.get_arg("interval")
        self.index = self.helper.get_arg("index")
        self._account = self.helper.get_arg("global_account")
        self.api_version = self.helper.get_arg("api_version")
        self.log_type = self.helper.get_arg("log_type")
        self.start_time = self.helper.get_arg("start_time") if self.helper.get_arg(
            "start_time") else "1970-01-01T00:00:00Z"
        self.sourcetype ="bitglass:{}".format(self.log_type)


    def _wait_for_kvstore(self):
        """Wait for KV store to initialize.
        KV store is used for maintaining time checkpoints for all the different exports.

        Raises:
            Exception: when kv store is not in ready state
        """
        def get_status():
            _, content = rest.simpleRequest("/services/kvstore/status",
                                            sessionKey=session_key,
                                            method="GET",
                                            getargs={"output_mode": "json"},
                                            raiseAllErrors=True)
            data = json.loads(content)["entry"]
            return data[0]["content"]["current"].get("status")

        session_key = self.helper.context_meta["session_key"]
        counter = 0
        status = get_status()
        self.helper.log_debug("KV Status: {}".format(status))
        while status != "ready":
            if status == "starting":
                counter += 1
                if counter < 3:
                    time.sleep(30)
                    status = get_status()
                    continue
            self.helper.log_error("KV store is not in ready state. Current state: " + str(status))
            raise Exception(
                "KV store is not in ready state. Current state: " + str(status))

    def get_checkpoint(self):
        """Return checkpoint based on export type and time filter field.
        """
        check_point_name = self.input_name
        self.helper.log_debug(
            "Checkpoint name is {}".format(check_point_name))

        state = self.helper.get_check_point(check_point_name)
        self.helper.log_debug(
            "Checkpoint state returned is {}".format(state))

        # in case if checkpoint is not found state value will be None,
        # so we are setting it to empty dict
        if not isinstance(state, dict):
            state = {}
        return state

    def save_checkpoint(self, time):
        """Save checkpoint state with name formed from input name.
        """
        self.helper.save_check_point(self.input_name, time)
        self.helper.log_debug(
            "Check point state saved is " + str(time))

    def _get_logs(self, start_date=None, next_page_token=None):
        """Request Bitglass API for logs of specified log_type

        Args:
            start_date (str): str of most earliest time to be retrieved format %Y-%m-%dT%H:%M:%SZ
            next_page_token (str): str to use for requesting next page of results

        Returns:
           events (dict): {next_page_token:"New Next Page Value", logs:[List of logs]}
        """
        ##TODO clean this up, it should not be in the function
        log_type=self.log_type
        cv=self.api_version
        result={"status":"complete", "next_page_token":""}
        ##Constructe the url
        ## TODO: Fix url creater to use the user
        url = utility.create_url("portal.bitglass.com", log_type, cv, start_date, next_page_token)
        headers={'Authorization':"Bearer {}".format(self._account["auth_token"]), "Host":"portal.bitglass.com"}
        self.helper.log_debug(url)
        try:
            response = requests.get(url, headers=headers)
        except Exception as e:
            self.helper.log_error("Broken response " + str(e))
        try:
           status_code=response.status_code
           jsonified=response.json()
           if status_code==429 :
              self.helper.log_error("Error 429 Exceeded Daily Limit")
              ##Save off checkpoint
           elif status_code==503:
              self.helper.log_error("Error 503 Internal Server Error")
              ##Issue with bitglass API save checkpoint try again at next scheduled time
           elif status_code==403:
              self.helper.log_error("Error 403 Unauthorized, token or credentials with insuficient permisssions")
              ##API token or credentials with insuficient permisssions
           elif status_code==401:
              self.helper.log_error("Error 401 Invalid Credentials or Token")
              ##Invalid token or credentials
           elif status_code==400:
              self.helper.log_error("Error 400: {}".format(jsonified))
              ##Issue with bitglass API save checkpoint try again at next scheduled time
           elif status_code==200:
              for event in jsonified['response']['data']:
                  self.write_event(event, self.sourcetype)
              if self.extract_token_time(jsonified['nextpagetoken'])!="":
                  result={"status":"incomplete","next_page_token":jsonified['nextpagetoken']}
              else:
                  result={"status":"complete","next_page_token":jsonified['nextpagetoken']}
           else:
              self.helper.log_error("Unknown Status code {} returned by {}: ".format(str(status_code), url))
        except Exception as e:
            self.helper.log_error("Broken property retrival " + str(e))

        return result

    def extract_token_time(self, token):
        '''Extracts the datetime field from the Bitglass next_page_token, token contains non-sensitive data

        Args:
            token (str): base64 encoded Bitglass nextpagetoken

        Returns:
           token_time (str): Time formated YYYY-MM-DDTHH:mm:ssZ OR empty string on error
        '''
        token_time = ""
        try:
            decoded = json.loads(base64.b64decode(token))
            assert('datetime' in decoded), 'No datetime in encoded returned token'
            ##Retreive time and format it
            token_time = datetime.datetime.strptime(decoded['datetime'], '%Y-%m-%dT%H:%M:%S.%fZ').strftime(TIME_PATTERN)
        except Exception as e:
            self.helper.log_error("Token extraction exception: {}".format(e))
        return token_time

    def _event_transformer(self, event):
        """Transforms, modifies and updates the received json event.

        Args:
            event (dict): access, admin, cloudaudit or cloudsummary event

        Returns:
            dict, int: transformed event and epoch time on which to index the event
        """
        try:
          event["address"] = self._account["address"]
          #"syslogheader": "<110>1 2020-04-07T13:37:06.371000Z api.bitglass.com NILVALUE NILVALUE access"
          syslogheader = event.pop("syslogheader", "")
          event_time = arrow.get(syslogheader.split(" ")[1]).timestamp
        except Exception as e:
          event_time = arrow.utcnow().timestamp
          self.helper.log_error("Transfrom Error: {}".format(str(e)))

        return event, event_time

    def write_event(self, event, sourcetype):
        """Index the event into the splunk into given sourcetype.
        Events are transformed first before ingesting into the splunk.

        Args:
            event (dict): event to index
            sourcetype (str): sourcetype in which to index the events
        """
        try:
            event, event_time = self._event_transformer(event)
            parsed_event = json.dumps(event)
            self.helper.log_debug("Parsed Event Length: " + str(len(parsed_event)))
            self.helper.log_debug("Index: {} \n Sourcetype: {} \n Time: {}".format(self.index, sourcetype, event_time))
            self.helper.log_debug("Parsed Event: " + parsed_event)
            event_obj = self.helper.new_event(
                data=parsed_event, time=event_time, index=self.index, sourcetype=sourcetype, unbroken=True)
            self.event_writer.write_event(event_obj)
        except Exception as e:
            self.helper.log_error("Disk Write Error: {}".format(e))

    def collect_events(self):
        """Collect Bitglass logs
           Loops until either error thrown by Bitglass API or till a blank page is returned
        """
        complete=False
        next_page_token=None
        self.helper.log_info("Bitglass data collection started for input: {}".format(self.input_name))
        try:
          self.start_time=self.get_checkpoint()['time']
          self.helper.log_debug("Got Bitglass {} checkpoint: {}".format(self.input_name, self.start_time))
        except:
          self.helper.log_debug("No Checkpoint set for {}, using input default {}".format(self.input_name, self.start_time))
        try:
            while complete==False:
                self.helper.log_debug("Bitglss next_page_token: {}".format(next_page_token))
                if next_page_token:
                    self.helper.log_debug("Fetching logs with next_page_token: {}".format(next_page_token))
                    result = self._get_logs(next_page_token=next_page_token)
                else:
                    self.helper.log_debug("Fetching logs with start_time")
                    result = self._get_logs(start_date=self.start_time)
                #Check if complete or if next_page_token is the same as the previous one
                if (result["status"]=="complete" or result["next_page_token"]==next_page_token):
                    self.helper.log_debug("Bitglass Collection Complete {}, saving checkpoint".format(self.input_name))
                    end_time = self.extract_token_time(result["next_page_token"])
                    if end_time:
                        ##If there is an end_time returned by the token
                        self.save_checkpoint({"time":end_time})
                    else:
                        ##If error collecting logs and next token missing
                        if self.last_time:
                            self.helper.log_debug("No next_page_token using last known time: {}".format(self.last_time))
                            self.save_checkpoint({"time":self.last_time})
                        else:
                            self.helper.log_debug("No next_page_token using pervious start time: {}".format(self.start_time))
                            self.save_checkpoint({"time":self.start_time})
                    complete=True
                else:
                    self.helper.log_debug("Bitglass Collection Incomplete, next_page: {}".format(result["next_page_token"]))
                    next_page_token=result["next_page_token"]
        except Exception as e:
            self.helper.log_error("Bitglss exports error occured during get log_type: {}".format(str(e)))
        #log that we completed with everything
        self.helper.log_info("Bitglass data collection completed for input: {}".format(self.input_name))
