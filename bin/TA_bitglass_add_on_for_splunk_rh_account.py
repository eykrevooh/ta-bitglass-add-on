
import ta_bitglass_add_on_for_splunk_declare

from bitglass_account_validation import *

from splunktaucclib.rest_handler.endpoint import (
    field,
    validator,
    RestModel,
    SingleModel,
)
from splunktaucclib.rest_handler import admin_external, util
from splunk_aoblib.rest_migration import ConfigMigrationHandler

util.remove_http_proxy_env_vars()


class AccountModel(SingleModel):
    def validate(self, name, data, existing=None):
        data_to_validate = data.copy()
        data_to_validate['name'] = name
        super(AccountModel, self).validate(name, data_to_validate, existing)

fields = [
    field.RestField(
        'address',
        required=True,
        encrypted=False,
        default=None,
        validator=Address()
    ),
    field.RestField(
        'auth_token',
        required=False,
        encrypted=True,
        default=None,
        validator=validator.String(
            min_len=1,
            max_len=8192,
        )
    ),
    field.RestField(
        'verify_ssl',
        required=False,
        encrypted=False,
        default=True,
        validator=None
    )
]
model = RestModel(fields, name=None)

endpoint = AccountModel(
    'ta_bitglass_add_on_for_splunk_account',
    model,
)


if __name__ == '__main__':
    admin_external.handle(
        endpoint,
        handler=ConfigMigrationHandler,
    )
