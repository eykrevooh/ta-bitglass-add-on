import io
import os
import re
import json
import requests
import datetime
import croniter
import six.moves.configparser
import time
import calendar
from math import ceil

import splunk.rest as rest
from splunk.clilib.bundle_paths import make_splunkhome_path

def get_configuration(my_app, file, folder="local"):
    conf_parser = six.moves.configparser.ConfigParser()
    conf = os.path.join(make_splunkhome_path(
        ["etc", "apps", my_app, folder, file]))
    stanzas = []
    if os.path.isfile(conf):
        with io.open(conf, 'r', encoding='utf_8_sig') as conffp:
            conf_parser.readfp(conffp)
        stanzas = conf_parser.sections()
    return conf_parser, stanzas

def get_app_version(my_app):
    config, stanza = get_configuration(my_app, 'app.conf', folder='default')
    return '{}-b{}'.format(
        config.get('launcher', 'version'),
        config.get('install', 'build')
    )

def get_account_data(global_account, my_app):
    account_config, account_stanzas = get_configuration(
        my_app, "ta_bitglass_add_on_for_splunk_account.conf")
    account_dict = {}

    for stanza in account_stanzas:
        if str(stanza) == global_account:
            account_dict["address"] = account_config.get(stanza, 'address')
            account_dict["verify_ssl"] = account_config.get(
                stanza, 'verify_ssl')
            account_dict["bitglass_account_type"] = account_config.get(
                stanza, 'bitglass_account_type')
            set_proxy_attributes(account_config, account_dict, stanza)

    return account_config, account_dict

def create_url(base_address, log_type, cv, start_date, next_page_token):
    url=""
    ##Validate the Input, Raise Error if invalid
    if (start_date==None and next_page_token==None):
        raise Exception(
            "Invalid Invocation of create_url utility both start_date and next_page_token cannot be None")
    elif (start_date!=None and next_page_token!=None):
        raise Exception(
            "Invalid Invocation of create_url utility both start_date and next_page_token cannot be set")
    ##Construct the url
    if start_date!=None:
        url = 'https://{}/api/bitglassapi/logs/v1/?type={}&cv={}&responseformat=json&startdate={}'.format(base_address, log_type, cv, start_date)
    else:
        url = 'https://{}/api/bitglassapi/logs/v1/?type={}&cv={}&responseformat=json&nextpagetoken={}'.format(base_address, log_type, cv, next_page_token)
    return url

def get_kvstore_status(session_key):
    _, content = rest.simpleRequest("/services/kvstore/status", sessionKey=session_key,
                                    method="GET", getargs={"output_mode": "json"}, raiseAllErrors=True)
    data = json.loads(content)['entry']
    return data[0]["content"]["current"].get("status")


def check_kvstore_status(session_key):
    status = get_kvstore_status(session_key)
    count = 0
    while status != 'ready':
        if status == 'starting':
            count += 1
            if count < 3:
                time.sleep(30)
                status = get_kvstore_status(session_key)
                continue
        raise Exception(
            "KV store is not in ready state. Current state: " + str(status))
