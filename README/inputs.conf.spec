[bitglass_input://<name>]
python.version = python3
global_account =
start_time = The date (UTC in \"YYYY-MM-DDThh:mm:ssZ\" format) from when to start collecting the data. Default value taken will be start of epoch time.
api_version = The API version of the Bitglass Endpoint
log_type = Bitglass endpoint log type (ie. Access, Admin, CloudAudit, CloudSummary)
